// preprocessor directive
#include <iostream>

// start of your program
int main()
{
    using std::cout;
    using std::endl;

    // write to the screen using std::cout
    cout << "Hello World" << endl;

    // return value to the os
    return 0;
}

