// preprocessor directive that inlcudes header iostream
#include <iostream>

// start of your program
int main()
{
    // write to screen
    std::cout << "Hello World" << std::endl;

    // return a value to the OS
    return 0;
}
