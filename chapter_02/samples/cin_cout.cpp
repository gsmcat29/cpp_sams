#include <iostream>
#include <string>
using namespace std;

int main()
{
    // declare a variable to store an integer
    int inputNumber;

    cout << "Enter an integer: ";

    // store integer given user input
    cin >> inputNumber;

    // the same with text, string data
    cout << "Enter your name: ";
    string inputName;
    cin >> inputName;

    cout << inputName << " entered " << inputNumber << endl;

    return 0;
}
