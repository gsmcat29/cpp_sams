// preprocessor directive
#include <iostream>

// start of your program
int main()
{
    // tell the compiler what namespace to search in 
    using namespace std;

    // write to the screen using std::cout
    cout << "Hello World" << endl;

    // return value to the OS
    return 0;
}