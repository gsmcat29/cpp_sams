// Modify Listing 2.4 to demonstrate subtraction (using –) and multiplication
// (using *).
#include <iostream>
using namespace std;

// declare function
int DemoConsoleOutput();

int main()
{
    // call, invoke the functin
    DemoConsoleOutput();

    return 0;
}

// define, implement the previously declared function
int DemoConsoleOutput()
{
    cout << "This is a simple string literal" << endl;
    cout << "Writing number five: " << 5 << endl;
    cout << "Performing division 10/5 = " << 10/5 << endl;
    cout << "PI when approximated is 22/7 = " << 22/7 << endl;
    cout << "PI is 22/7 = " << 22.0/7 << endl;

    cout << "subtraction = " << 22 - 7 << endl;
    cout << "multiplication = " << 22 * 7 << endl;

    return 0;
}