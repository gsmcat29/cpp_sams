// Modify enum YourCards in quiz question 4 to demonstrate that the value of
// Queen can be 45.

#include <iostream>
using namespace std;

enum YourCards
{
    Ace  = 43,
    Jack,
    Queen,
    King
};

int main()
{
    cout << "Displaying  values " << endl;
    cout << "Ace: " << Ace << endl;
    cout << "Jack: " << Jack << endl;
    cout << "Queen: " << Queen << endl;
    cout << "King: " << King << endl;

    return 0;
}