/*
Write a program to calculate the area and circumference of a circle where the
radius is fed by the user.
*/

#include <iostream>
using namespace std;

const int PI = 3.141592;

int main()
{
    double radius = 0.0;
    double area = 0.0;
    double circumference = 0.0;

    cout << "Please enter radius of circle: ";
    cin >> radius;

    cout << "--- Circumference of circle:\n";
    circumference = 2 * PI * radius;
    cout << circumference << endl;
    cout << "--- Area of circle:\n";
    area = PI * radius * radius;
    cout << area << endl;

    return 0;
}