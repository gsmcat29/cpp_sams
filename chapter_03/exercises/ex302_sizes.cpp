/*
Write a program that demonstrates that the size of an unsigned integer and a 
normal integer are the same, and that both are smaller in size than a long 
integer.
*/

#include <iostream>
using namespace std;

int main()
{
    cout << "sizeof unsigned integer: " << sizeof(unsigned int) << endl;
    cout << "sizeof integer:          " << sizeof(int) << endl;
    cout << "sizeof long integer:     " << sizeof(long int) << endl;

    return 0;
}