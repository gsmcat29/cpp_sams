/*
In Exercise 3, if the area and circumference were to be stored in integers, how
would the output be any different?
*/

/*
Answer: The values would be truncated
*/

#include <iostream>
using namespace std;

const int PI = 3.141592;

int main()
{
    double radius = 0.0;
    int area = 0.0;
    int circumference = 0.0;

    cout << "Please enter radius of circle: ";
    cin >> radius;

    cout << "--- Circumference of circle:\n";
    circumference = 2 * PI * radius;
    cout << circumference << endl;
    cout << "--- Area of circle:\n";
    area = PI * radius * radius;
    cout << area << endl;

    return 0;
}