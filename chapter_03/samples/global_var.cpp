#include <iostream>
using namespace std;

// three global integers
int firstNumber = 0;
int secondNumber = 0;
int multiplicationResult = 0;

#include <iostream>
using namespace std;

void MultiplyNumbers()
{
    cout << "Enter the first number: ";
    int firstNumber = 0;
    cin >> firstNumber;

    cout << "Enter the second number: ";
    int secondNumber = 0;
    cin >> secondNumber;

    // multiply two numbers, store result in a variable
    int multiplicationResult = firstNumber * secondNumber;

    // display result
    cout << "Displaying from MultiplyNumbers(): ";
    cout << firstNumber << " x " << secondNumber;
    cout << " = " << multiplicationResult << endl;
}

int main()
{
    cout << "This program will help you multiply two numbers" << endl;

    // call the function that does all the work
    MultiplyNumbers();

    cout << "Displating from main(): ";

    // This line will now compile and work!
    cout << firstNumber << " x " << secondNumber;
    cout << " = " << multiplicationResult << endl;

    return 0;
}