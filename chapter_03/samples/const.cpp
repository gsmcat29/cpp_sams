#include <iostream>
using namespace std;

int main()
{
    const double pi = 22.0 / 7;
    cout << "The value of constant pi is " << pi << endl;

    // uncomment next line to view compile failure
    // pi = 345;

    return 0;
}